import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistroRoutingModule } from './registro-routing.module';
import { RegistroCreateComponent } from './registro-create/registro-create.component';
import { RegistroListComponent } from './registro-list/registro-list.component';
import { MainComponent } from './main/main.component';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@NgModule({
  declarations: [RegistroCreateComponent, RegistroListComponent, MainComponent],
  imports: [
    CommonModule,
    RegistroRoutingModule,
    ReactiveFormsModule,
    HttpClient
  ]
})
export class RegistroModule { }
