import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistroCreateComponent } from './registro-create/registro-create.component';
import { RegistroListComponent } from './registro-list/registro-list.component';

const routes: Routes = [

  {
    path: 'registro-create',
    component: RegistroCreateComponent
  },
  {
    path: 'registro-list',
    component: RegistroListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistroRoutingModule { }
