import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {createRequestParam} from '../util/request.utils';
import { environment } from 'src/environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(private http: HttpClient) { 
  }

  public queryRegistro(req?: any): Observable<Iregistro[]> {
    let params = createRequestParam(req);
    return this.http.get<Iregistro[]>(`${environment.ENN_POINT}/api/registro`, {params: params})
    .pipe (map(res =>{
      return res;
    }))
  }

  public saveRegistro(registro:Iregistro)
}
