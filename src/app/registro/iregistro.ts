export interface Iregistro {
    id?: number,
    fechaDia: string,
    placa: string,
    comprador: string,
    telComprador: string,
    direccion: string,
    email: string,
    docIdentidad: string,
    fechaFinal: string,
    precio?: number

}
