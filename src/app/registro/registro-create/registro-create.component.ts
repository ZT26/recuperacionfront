import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegistroService} from '../registro.service';


@Component({
  selector: 'app-registro-create',
  templateUrl: './registro-create.component.html',
  styleUrls: ['./registro-create.component.styl']
})
export class RegistroCreateComponent implements OnInit {

  registroCreateFormGroup: FormGroup = this.formBuilder.group({

comprador: ['', Validators.compose([Validators.required, Validators.maxLength(25)])],
docIdentidad: ['', Validators.compose([Validators.required, Validators.maxLength(10)])],

  })

  constructor(private formBuilder: FormBuilder, private registroService: RegistroService) {

   }

  ngOnInit(): void {
  }


  saveRegistro(): void {
    console.warn('datos', this.registroCreateFormGroup.value)
    this.registroService.saveRegistro(this.registroCreateFormGroup.value)
      .subscribe(res => {
        console.warn('Save ok', this.registroCreateFormGroup.value)
      }, error => console.error('error in save registro', error))
  }

}
