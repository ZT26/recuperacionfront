import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCreateComponent } from './registro-create.component';

describe('RegistroCreateComponent', () => {
  let component: RegistroCreateComponent;
  let fixture: ComponentFixture<RegistroCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
